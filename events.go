package hikevents

import (
	"encoding/xml"
	"errors"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"regexp"
	"strings"
	"time"
)

const (
	eventBuff     = 10
	errBuff       = 10
	sessionTime   = time.Second * 20
	errorCooldown = time.Second
)

var boundaryRe = regexp.MustCompile(`boundary\=(\S+)`)

var client = http.Client{
	Timeout: sessionTime,
}

type Event struct {
	XMLName         xml.Name `xml:"EventNotificationAlert"`
	EventType       string   `xml:"eventType"`
	EventState      string   `xml:"eventState"`
	ChannelID       int      `xml:"channelID"`
	ActivePostCount int      `xml:"activePostCount"`
}

type Listener struct {
	host     string
	user     string
	password string

	EventChan chan (Event)
	ErrChan   chan (error)
}

func NewListener(host string, user string, password string) (*Listener, error) {
	listener := &Listener{
		host:     host,
		user:     user,
		password: password,

		EventChan: make(chan Event, eventBuff),
		ErrChan:   make(chan error, errBuff),
	}

	err := listener.checkConnection()
	if err != nil {
		return nil, err
	}

	go listener.listen()
	return listener, nil
}

func (l *Listener) NextEvent() Event {
	return <-l.EventChan
}

func (l *Listener) listen() {
	for {
		err := l.session()
		if err != nil {
			select {
			case l.ErrChan <- err:
				time.Sleep(errorCooldown)
			default:
			}
		}
	}
}

func (l *Listener) checkConnection() error {
	req, err := l.prepareRequest()
	if err != nil {
		return err
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusUnauthorized {
		return errors.New("Wrong username or password")
	}
	if resp.StatusCode != http.StatusOK {
		return errors.New("Unexpected http status: " + resp.Status)
	}
	_, err = extractBoundary(resp.Header)
	resp.Body.Close()
	return err
}

func (l *Listener) prepareRequest() (*http.Request, error) {
	req, err := http.NewRequest("GET", "http://"+l.host+"/ISAPI/Event/notification/alertStream", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(l.user, l.password)
	req.Header.Add("Accept", "multipart/x-mixed-replace")
	return req, nil
}

func extractBoundary(h http.Header) (string, error) {
	contentType := h.Get("Content-Type")
	if strings.HasPrefix(contentType, "multipart/mixed;") {
		matches := boundaryRe.FindStringSubmatch(contentType)
		if len(matches) > 1 {
			return matches[1], nil
		} else {
			return "", errors.New("Cannot find boundary")
		}
	} else {
		return "", errors.New("Wrong content type")
	}
}

func (l *Listener) session() error {
	start := time.Now()
	req, err := http.NewRequest("GET", "http://"+l.host+"/ISAPI/Event/notification/alertStream", nil)
	if err != nil {
		return err
	}
	req.SetBasicAuth(l.user, l.password)
	req.Header.Add("Accept", "multipart/x-mixed-replace")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	boundary, err := extractBoundary(resp.Header)
	if err != nil {
		return err
	}

	rd := multipart.NewReader(resp.Body, boundary)
	for {
		part, err := rd.NextPart()
		if err != nil {
			return err
		}

		data, err := ioutil.ReadAll(part)
		if err != nil {
			return err
		}

		event := Event{}
		err = xml.Unmarshal(data, &event)
		if err != nil {
			return err
		}

		l.EventChan <- event

		if time.Since(start) > sessionTime-time.Second {
			return nil
		}
	}
}
