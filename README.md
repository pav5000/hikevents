# hikevents
Event stream client for Hikvision cameras

Tested on DS-2CD1321-I but should work with the most other Hikvision IP cameras.
You can use this code to set you triggers on motion detection.

## Examples

### Simpliest client
```
package main

import (
	"fmt"
	"github.com/pav5000/hikevents"
)

func main() {
	listener, err := hikevents.NewListener("192.168.1.64", "admin", "1234")
	if err != nil {
		fmt.Println("Error connecting to camera:", err)
		return
	}
	for {
		event := listener.NextEvent()
		fmt.Println(event.EventType, event.EventState)
	}
}

```

### With error handling
```
package main

import (
	"fmt"
	"github.com/pav5000/hikevents"
)

func main() {
	listener, err := hikevents.NewListener("192.168.1.64", "admin", "1234")
	if err != nil {
		fmt.Println("Error connecting to camera:", err)
		return
	}
	for {
		select {
		case event := <-listener.EventChan:
			fmt.Println("Event:", event.EventType, event.EventState)
		case err := <-listener.ErrChan:
			fmt.Println("Error:", err)
		}
	}
}

```

## Event types

I'm getting 2 types of events from my camera. The first one is `videoloss` which is always inactive and is generated every second. The second one is `VMD` which is generated when the camera detects motion (if motion detection is enabled).
